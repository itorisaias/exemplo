const mongoose = require('mongoose')
const env = process.env
const conn = `mongodb://${env.MONGO_USER}:${env.MONGO_PASSWORD}@${env.MONGO_HOST}:${env.MONGO_PORT}/${env.MONGO_DB}`

mongoose.Promise = global.Promise

module.exports = mongoose.connect(conn, { useMongoClient: true })
