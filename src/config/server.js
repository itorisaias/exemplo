const express = require('express')
const bodyParser = require('body-parser')
const expressActuator = require('express-actuator')
const morgan = require('morgan')
const allowCors = require('./cors')

const middlewareChecktoken = require('./../helpers/middleware.checktoken')
const logger = require('./winston')

const server = express()
const port = process.env.PORT || 4000

server.use(allowCors)
server.use(morgan('dev'))
server.use(bodyParser.urlencoded({ extended: true }))
server.use(bodyParser.json())
// server.use(morgan('combined', { stream: logger.stream }))
server.use(expressActuator('/management'))
server.use(middlewareChecktoken)
// localhost:4000/management/metrics
// localhost:4000/management/info

server.listen(port, () =>
    logger.info(`BACKEND is running on port ${port}.`))

module.exports = server
