const express = require('express')
const fs = require('fs')

module.exports = function (server) {

    // API Routes
    const router = express.Router()
    server.use('/api', router)

    // TODO Routes
    const entidades = fs.readdirSync(__dirname + '/../api')
    entidades.forEach(entidade =>
        require(`../api/${entidade}/${entidade}Service`).register(router, `/${entidade}s`))
}