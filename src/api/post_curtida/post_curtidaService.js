const Post_Curtida = require('./post_curtida')

Post_Curtida.methods(['get', 'post', 'put', 'delete'])
Post_Curtida.updateOptions({ new: true, runValidators: true })

module.exports = Post_Curtida
