const restful = require('node-restful')
const mongoose = restful.mongoose

const post_curtidaSchema = new mongoose.Schema({
  id_usuario: { type: 'ObjectId', ref: 'usuario', required: true },
  id_post: { type: 'ObjectId', ref: 'post', required: true }
})

module.exports = restful.model('Post_Curtida', post_curtidaSchema)
