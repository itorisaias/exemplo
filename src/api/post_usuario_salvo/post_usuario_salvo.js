const restful = require('node-restful')
const mongoose = restful.mongoose

const post_usuario_salvoSchema = new mongoose.Schema({
  id_post: { type: 'ObjectId', ref: 'post', required: true },
  id_usuario: { type: 'ObjectId', ref: 'usuario', required: true }
})

module.exports = restful.model('Post_Usuario_Salvo', post_usuario_salvoSchema)
