const Post_Usuario_Salvo = require('./post_usuario_salvo')

Post_Usuario_Salvo.methods(['get', 'post', 'put', 'delete'])
Post_Usuario_Salvo.updateOptions({ new: true, runValidators: true })

module.exports = Post_Usuario_Salvo
