const Usuario_Usuario = require('./usuario_usuario')

Usuario_Usuario.methods(['get', 'post', 'put', 'delete'])
Usuario_Usuario.updateOptions({new: true, runValidators: true})

module.exports = Usuario_Usuario
