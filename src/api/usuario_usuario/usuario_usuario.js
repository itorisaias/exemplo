const restful = require('node-restful')
const mongoose = restful.mongoose

const usuario_usuarioSchema = new mongoose.Schema({
    id_usuario_follow: { type: 'ObjectId', ref: 'usuario', required: true },
    id_usuario_followed: { type: 'ObjectId', ref: 'usuario', required: true },
})

module.exports = restful.model('Usuario_Usuario', usuario_usuarioSchema)
