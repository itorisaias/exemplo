const Login = require('./login')
const crypt = require('crypto')
const Usuario = require('./../usuario/usuario')
const jwt = require('./../../helpers/jwt')
const nodemailer = require('./../../helpers/nodemailer.js')

// Criptografa a senha e retorna o hash gerado
const hash_password = senha =>
  crypt.createHash('md5').update(senha).digest("hex")

// Rota de `/api/logins/autenticacao` com o metodo POST
Login.route('autenticacao.post', (req, res, next) => {

  // Verifica se existe `username` e `senha` no body da requisição
  let { username, senha } = req.body
  if (!username && !senha)
    return res.status(203).json({ message: 'Payload is not valid.' })

  // Busca no banco um usuario que tenha username e senha(criptografada)
  Usuario.findOne({ username, senha: hash_password(senha) }, (err, user) => {
    // Caso tenha problema com o banco
    if (err) {
      return res.status(500).json({ message: 'Problem with the service.' })
    }

    // Caso não ache o registro
    if (!user) {
      return res.status(203).json({ message: 'Login is not valid.' })
    }

    // Usuario encontrado e criação o token e guarda no cabeçalho da resposta
    // E response com sucesso
    res.setHeader('Authorization', jwt.createToken({
      _id: user._id,
      email: user.email
    }))
    return res.status(200).json({ message: 'Login valid.' })
  })
});


Login.route('recuperarsenha.post', (req, res, next) => {

  // Verifica se existe `username` e `senha` no body da requisição
  let { email } = req.body
  if (!email)
    return res.status(203).json({ message: 'Payload is not valid.' })

  // Busca no banco um usuario que tenha username e senha(criptografada)
  Usuario.findOne({ email }, (err, user) => {
    // Caso tenha problema com o banco
    if (err) {
      return res.status(500).json({ message: 'Problem with the service.' })
    }

    // Caso não encontrou o registro
    if (!user) {
      return res.status(203).json({ message: 'Conta inesistente.' })
    }

    nodemailer(email)
      .then(send => res.status(200).json({ message: `Nova senha enviada para o email ${user.email}.` }))
      .catch(error => res.status(500).json({ message: 'Problem with the service.' }))
  })
});

module.exports = Login
