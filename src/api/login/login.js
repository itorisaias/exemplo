const restful = require('node-restful')
const mongoose = restful.mongoose

const loginSchema = new mongoose.Schema({
  username: { type: String, required: true },
  senha: { type: String, required: true }
})

module.exports = restful.model('Login', loginSchema)
