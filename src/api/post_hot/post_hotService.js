const Post_Hot = require('./post_hot')

Post_Hot.methods(['get', 'post', 'put', 'delete'])
Post_Hot.updateOptions({ new: true, runValidators: true })

module.exports = Post_Hot
