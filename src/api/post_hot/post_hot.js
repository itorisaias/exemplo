const restful = require('node-restful')
const mongoose = restful.mongoose

const post_hotSchema = new mongoose.Schema({
  id_post: { type: 'ObjectId', ref: 'post', required: true }
})

module.exports = restful.model('Post_Hot', post_hotSchema)
