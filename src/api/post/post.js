const restful = require('node-restful')
const mongoose = restful.mongoose

const postSchema = new mongoose.Schema({
  descricao: { type: String, required: true },
  id_categoria: { type: 'ObjectId', ref: 'categoria', required: true },
  id_usuario: { type: 'ObjectId', ref: 'usuario', required: true },
  img: { type: String, required: true },
  datahora: { type: Date, required: true, default: Date.now },
  localização: { type: String, required: true },
  status: { type: Boolean, required: true, default: true }
})

module.exports = restful.model('Post', postSchema)
