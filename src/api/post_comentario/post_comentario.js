const restful = require('node-restful')
const mongoose = restful.mongoose

const post_comentarioSchema = new mongoose.Schema({
  id_post: { type: 'ObjectId', red: 'post', required: true },
  descricao: { type: String, required: true },
  data: { type: Date, required: true, default: Date.now }
})

module.exports = restful.model('Post_Comentario', post_comentarioSchema)
