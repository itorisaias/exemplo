const Post_Comentario = require('./post_comentario')

Post_Comentario.methods(['get', 'post', 'put', 'delete'])
Post_Comentario.updateOptions({ new: true, runValidators: true })

module.exports = Post_Comentario
