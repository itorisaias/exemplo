const restful = require('node-restful')
const mongoose = restful.mongoose

const categoriaSchema = new mongoose.Schema({
    descricao: { type: String, required: true },
})

module.exports = restful.model('Categoria', categoriaSchema)
