const Sub_Categoria = require('./sub_categoria')

Sub_Categoria.methods(['get', 'post', 'put', 'delete'])
Sub_Categoria.updateOptions({new: true, runValidators: true})

module.exports = Sub_Categoria
