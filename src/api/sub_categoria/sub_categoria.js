const restful = require('node-restful')
const mongoose = restful.mongoose

const sub_categoriaSchema = new mongoose.Schema({
    descricao: { type: String, required: true },
    id_categoria: { type: 'ObjectId', ref: 'categoria', required: true }    
})

module.exports = restful.model('Sub_Categoria', sub_categoriaSchema)
