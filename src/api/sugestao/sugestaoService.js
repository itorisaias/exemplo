const Sugestao = require('./sugestao')

Sugestao.methods(['get', 'post', 'put', 'delete'])
Sugestao.updateOptions({new: true, runValidators: true})

module.exports = Sugestao
