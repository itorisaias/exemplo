const restful = require('node-restful')
const mongoose = restful.mongoose

const sugestaoSchema = new mongoose.Schema({
    id_categoria: { type: 'ObjectId', ref: 'categoria', required: true },
    id_usuario: { type: 'ObjectId', ref: 'usuario', required: true }
})

module.exports = restful.model('Sugestao', sugestaoSchema)
