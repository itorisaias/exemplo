const Usuario = require('./usuario')
const crypt = require('crypto')
const md5 = require('md5')
const nodemailer = require('./../../helpers/nodemailer')

const hash_password = (req, res, next) => {
  req.body.senha = crypt.createHash('md5').update(req.body.senha).digest("hex");
  next();
}

const sendMail = (req, res, next) => {
  const emailSend = req.body.email
  // nodemailer(emailSend)
  next()
}

Usuario.methods(['get', 'post', 'put', 'delete'])
Usuario.updateOptions({ new: true, runValidators: true })

Usuario.before('post', hash_password).before('put', hash_password);
Usuario.after('post', sendMail)

module.exports = Usuario
