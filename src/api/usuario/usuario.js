const restful = require('node-restful')
const mongoose = restful.mongoose

const usuarioSchema = new mongoose.Schema({
  nome: { type: String, minlength: 4, maxlength: 50, required: true },
  email: { type: String, minlength: 8, maxlength: 250, unique: true, required: true },
  username: { type: String, minlength: 4, maxlength: 50, unique: true, required: true },
  senha: { type: String, minlength: 8, maxlength: 250, required: true },
  genero: { type: String, enum: ['masculino', 'feminino'] },
  foto: { type: String },
  cep: { type: String, minlength: 8, maxlength: 10 },
  cidade: { type: String, minlength: 3, maxlength: 250 },
  site: { type: String, minlength: 8, maxlength: 250 },
  biografia: { type: String, maxlength: 250 },
  facebook: { type: String },
  telefone: { type: String, minlength: 9, maxlength: 14 },
  created_date: { type: Date, default: Date.now },
  status: { type: String, enum: ['pending', 'ongoing', 'completed'], default: ['pending'] }
})

module.exports = restful.model('Usuario', usuarioSchema)