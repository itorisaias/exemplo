const jsonwebtoken = require('jsonwebtoken')

const secretkey = process.env.token_secret || Math.floor((Math.random * 9999) * Math.random).toString()

module.exports.createToken = payload => {
  try {
    const token = jsonwebtoken.sign(payload, secretkey, { expiresIn: `${process.env.TIME_EXPIRES_TOKEN}h` })
    return token
  } catch (err) {
    return null
  }
}
module.exports.verifyToken = token => {
  try {
    const valid = jsonwebtoken.verify(token, secretkey.toString())
    return valid
  } catch (err) {
    return false
  }
}
module.exports.decodeToken = token => {
  try {
    const payLoad = jsonwebtoken.decode(token)
    return payLoad
  } catch (err) {
    return null
  }
}