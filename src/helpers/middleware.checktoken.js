const jwt = require('./jwt')

// Array com rotas que não precisam de autenticação
const unauthenticatedRoutes = [
  { method: 'POST', url: '/api/logins/autenticacao' }
]

// Busca no array `unauthenticatedRoutes` se existe um registro igual ao requerido
// Caso não exista retorna nulo
// Caso exista retorna o registro
const needAuthentication = (method, url) =>
  unauthenticatedRoutes.find(routeMethod => routeMethod.method == method && routeMethod.url == url)

module.exports = (req, res, next) => {

  // Verifica se a rota acessar precisa de autenticação
  const { method, url } = req
  if (needAuthentication(method, url)) {
    return next()
  }

  // Verifica se existe um token no cabecalho da requisição
  const token = req.headers.Authorization || req.headers.authorization
  if (!token) {
    return res.status(403).json({ message: 'You are not logged in to this API.' })
  }

  // Verifica se o token é valido
  if (!jwt.verifyToken(token)) {
    return res.status(403).json({ message: 'Sent token is not valid.' })
  }

  // Cria um novo token e seta no cabecalho da resposta
  const payload = jwt.decodeToken(token)
  res.setHeader('Authorization', jwt.createToken({
    _id: payload._id,
    email: payload.email
  }))
  return next()
}
