const nodemailer = require('nodemailer')

module.exports = email => {
  return new Promise((res, rej) => {

    // create reusable transporter object using the default SMTP transport
    var transporter = nodemailer.createTransport({
      service: 'Gmail',
      auth: {
        user: process.env.NODEMAILER_USER,
        pass: process.env.NODEMAILER_PASSWORD
      }
    });

    // setup e-mail data with unicode symbols
    var mailOptions = {
      from: 'itor.isaias@gmail.com', // sender address
      to: email.toString(), // list of receivers
      subject: 'Hello ✔', // Subject line
      text: 'Hello world ?', // plaintext body
      html: '<b>Hello world ?</b>' // html body
    };

    // send mail with defined transport object
    transporter.sendMail(mailOptions, function (error, info) {
      if (error) {
        rej(error)
        return console.log(error);
      }
      res('Message sent: ' + info.response)
      console.log('Message sent: ' + info.response);
    })

  })
}