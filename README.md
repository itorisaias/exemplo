# BackEnd Seu Look

descrição da aplicação.

## Getting Started

Para roda-lo primeiro precisamos clonar o projeto:

```sh
git clone https://github.com/nataliasilva/backend.git
```

Logo após ter clonado o projeto renomeio o arquivo ``.env.exemple`` para ``.env`` e configurar com suas configurações.

### Prerequisites

* [NodeJS](https://nodejs.org/downloads) - Versão v6.11.x^

```sh
npm install -g npm@latest
```

### Installing

Após configurado basta apÃ©nas instalar suas dependencias.

```sh
npm install
```

### Initialize the application

initialize the application in development: ```npm run dev```

initialize the application in production: ```npm run prod```
